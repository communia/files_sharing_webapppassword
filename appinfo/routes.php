<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Aleix Quintana Alsius <kinta@communia.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\FilesSharingWebAppPassword\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
	'routes' => [
		/*
		 * OCS Share API
		 */
		[
			'name' => 'ShareAPI#getShares',
			'url' => '/api/v1/shares',
			'verb' => 'GET',
		],
		[
			'name' => 'ShareAPI#getInheritedShares',
			'url' => '/api/v1/shares/inherited',
			'verb' => 'GET',
		],
		[
			'name' => 'ShareAPI#createShare',
			'url' => '/api/v1/shares',
			'verb' => 'POST',
		],
		[
			'name' => 'ShareAPI#preflighted_cors',
			'url' => '/api/v1/shares',
			'verb' => 'OPTIONS',
		],
		[
			'name' => 'ShareAPI#pendingShares',
			'url' => '/api/v1/shares/pending',
			'verb' => 'GET',
		],
		[
			'name' => 'ShareAPI#getShare',
			'url' => '/api/v1/shares/{id}',
			'verb' => 'GET',
		],
		[
			'name' => 'ShareAPI#updateShare',
			'url' => '/api/v1/shares/{id}',
			'verb' => 'PUT',
		],
		[
			'name' => 'ShareAPI#deleteShare',
			'url' => '/api/v1/shares/{id}',
			'verb' => 'DELETE',
		]
	]
];
