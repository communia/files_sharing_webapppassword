<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Aleix Quintana Alsius <kinta@communia.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\FilesSharingWebAppPassword\AppInfo;

use OCP\AppFramework\App;

class Application extends App {
	public const APP_ID = 'files_sharing_webapppassword';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
