<!--
SPDX-FileCopyrightText: Aleix Quintana Alsius <kinta@communia.org>
SPDX-License-Identifier: CC0-1.0
-->
The same functionality is merged now in webapppassword, https://github.com/digital-blueprint/webapppassword/pull/61


# File Sharing Web App Password

This is a Nextcloud app that exposes parts of the share api to be used outside of the domain. It will use the domains defined by any of the allowed options of the [webapppassword app](https://github.com/digital-blueprint/webapppassword), it also can use the ones defined in `config/config.php`.

`'files_sharing_webapppassword.origins' => ['https://example.com']`

Under the hood it exposes parts of the sharing api (by now just the shares list, the preflight OPTIONS endpoint, and the createShare) in this url:

`https://example.com/index.php/apps/files_sharing_webapppassword/api/v1/shares`

Place this app in **nextcloud/apps/**

## Building the app

The app can be built by using the provided Makefile by running:

    make

This requires the following things to be present:
* make
* which
* tar: for building the archive
* curl: used if phpunit and composer are not installed to fetch them from the web
* npm: for building and testing everything JS, only required if a package.json is placed inside the **js/** folder

The make command will install or update Composer dependencies if a composer.json is present and also **npm run build** if a package.json is present in the **js/** folder. The npm **build** script should use local paths for build systems and package managers, so people that simply want to build the app won't need to install npm libraries globally, e.g.:

**package.json**:
```json
"scripts": {
    "test": "node node_modules/gulp-cli/bin/gulp.js karma",
    "prebuild": "npm install && node_modules/bower/bin/bower install && node_modules/bower/bin/bower update",
    "build": "node node_modules/gulp-cli/bin/gulp.js"
}
```


## Publish to App Store

First get an account for the [App Store](http://apps.nextcloud.com/) then run:

    make && make appstore

The archive is located in build/artifacts/appstore and can then be uploaded to the App Store.

## Running tests
You can use the provided Makefile to run all tests by using:

    make test

This will run the PHP unit and integration tests and if a package.json is present in the **js/** folder will execute **npm run test**

Of course you can also install [PHPUnit](http://phpunit.de/getting-started.html) and use the configurations directly:

    phpunit -c phpunit.xml

or:

    phpunit -c phpunit.integration.xml

for integration tests
